﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }
        
        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                if (item.Nom == nomHotel.Text)
                {
                    int Id = item.Id;
                    Persistance.ajouteChambre(Id, description.Text, (int)numEtage.Value);
                }
                nomHotel.Items.Add(item);
            }
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                nomHotel.Items.Add(item.Nom);
            }
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void raz()
        {
            this.numEtage.Value = 0;
            this.description.Text = "";
            this.nomHotel.Text = "";
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void nomHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
