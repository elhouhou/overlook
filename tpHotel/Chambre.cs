﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        private int num;
        private int id;
        private int etage;
        private string description;


        public Chambre(int unNum, int uneId, int unEtage, string uneDescription)
        {
            num = unNum;
            id = uneId;
            Etage = unEtage;
            Description = uneDescription;
        }

        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
        public int Num { get => num; set => num = value; }
        public int Id { get => id; set => id = value; }
    }
}
