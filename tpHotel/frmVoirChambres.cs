﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmVoirChambres : Form
    {
        public frmVoirChambres()
        {
            InitializeComponent();
        }
        

        private void frmVoirChambres_Load(object sender, EventArgs e)
        {
            lstChambres.DataSource = Persistance.getLesChambres();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lstChambres_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
